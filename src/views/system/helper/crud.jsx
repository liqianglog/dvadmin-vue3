// crud.js
import * as api from "./api";
import {dict} from "@fast-crud/fast-crud";
import { ref } from "vue";
import _ from "lodash-es";
function useSearchRemote() {
    const state = {
        data: ref([]),
        loading: ref(false)
    };

    const fetchUser = _.debounce(async (value) => {
        const data = []
        state.data.value = data;
        state.loading.value = false;
    }, 800);

    return {
        fetchUser,
        searchState: state
    };
}
// 构建crudOptions的方法
export default function ({expose}) {
    const pageRequest = async (query) => {
        return await api.GetList(query);
    };
    const editRequest = async ({form, row}) => {
        form.id = row.id;
        return await api.UpdateObj(form);
    };
    const delRequest = async ({row}) => {
        return await api.DelObj(row.id);
    };

    const addRequest = async ({form}) => {
        return await api.AddObj(form);
    };

    const { fetchUser, searchState } = useSearchRemote();

    return {
        crudOptions: {
            //请求配置
            request: {
                pageRequest, // 列表数据请求
                addRequest,  // 添加请求
                editRequest, // 修改请求
                delRequest,  // 删除请求
            },
            columns: {
                parent: {
                    title: "父级菜单",
                    type: "dict-cascader",
                    dict: dict({
                        isTree: true,
                        url: "/mock/dicts/cascaderData"
                    }),
                    form: {
                        component: {
                            filterable: true,
                            // props下配置属性跟配置在component下是一样的效果，而el-cascade下也有一个叫props的属性，所以需要配置两层
                            props: { props: { checkStrictly: true } }
                        }
                    }
                },
                name: {
                    title: "菜单名称", //字段名称
                    search: {show: true}, // 搜索配置
                    type: "text", // 字段类型
                },
                icon: {
                    title: "图标",
                    search: {show: true},
                    type: "text"
                },
                sort: {
                    title: "排序",
                    type: "number"
                },
                is_catalog: {
                    title: "是否目录",
                    type: "dict-switch",
                    dict: dict({
                        data: [
                            { value: true, label: "是" },
                            { value: false, label: "否" }
                        ]
                    })
                },
                is_link: {
                    title: "外链接",
                    type: "dict-radio",
                    dict: dict({
                        data: [
                            { value: true, label: "是" },
                            { value: false, label: "否" }
                        ]
                    }),
                    form: {
                        component: {
                            optionName: "el-radio-button"
                        }
                    }
                },
                web_path:{
                    title: "路由地址",
                    type: "text",
                    form:{
                        helper:"需要最前面加上/"
                    }
                },
                component:{
                    title: "组件地址",
                    type: "select",
                    form: {
                        // component: {
                        //     name: "fs-dict-select",
                        //     multiple: true,
                        //     filterable: true,
                        //     remote: true,
                        //     "reserve-keyword": true,
                        //     placeholder: "输入远程搜索",
                        //     options: searchState.data,
                        //     remoteMethod: (query) => {
                        //         if (query !== "") {
                        //             fetchUser();
                        //         } else {
                        //             searchState.data.value = [];
                        //         }
                        //     },
                        //     loading: searchState.loading
                        // }
                    }
                },
            },
            // 其他crud配置
        },
    };
}
